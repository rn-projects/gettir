export const Theme = {
  roundness: 2,
  colors: {
    primery: '#5D3EBD',
    success: '#CFCFCF',
    secondry: '#FFD10C',
    graylight: '#515151',
    light: '#F5F5F5',
    dark: '#222',
    white: '#fff',
    danger: '#3B579E',
    tablight: '#F2F0FD',
  },
};
