import React from 'react';
import {SafeAreaView, Text, View} from 'react-native';
import AppHeader from '../../Components/AppHeader';
import AppButton from '../../Components/Button';
import {Theme} from '../../theme/Theme';
import {styles} from './Style';

const Login = () => {
  return (
    <SafeAreaView>
      <AppHeader icon="arrow-left" title="getir" title1="more" />
      <View style={styles.login_layout}>
        <AppButton
          backgroundColor={Theme.colors.danger}
          title="connect with facebook"
        />
      </View>
    </SafeAreaView>
  );
};
export default Login;
