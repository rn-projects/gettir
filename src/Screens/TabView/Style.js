import {StyleSheet} from 'react-native';
import {Theme} from '../../theme/Theme';

export const styles = StyleSheet.create({
  home_view: {
    flex: 1,
    backgroundColor: Theme.colors.white,
  },

  // Tabs
  tab_scroll_view: {
    flex: 1,
    backgroundColor: Theme.colors.white,
    paddingHorizontal: 24,
    paddingTop: 24,
  },
  tab_header_view: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: Theme.colors.primery,
    paddingBottom: 7,
  },
  tab_header_title: {
    fontSize: 20,
    color: Theme.colors.primery,
    fontWeight: '700',
  },
  tab_catg_sub_view: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  tab_catg_sell_circle: {
    height: 8,
    width: 8,
    backgroundColor: Theme.colors.tbGreen,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: Theme.colors.primery,
  },
  tab_catg_buy_circle: {
    height: 8,
    width: 8,
    backgroundColor: Theme.colors.tbPink,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: Theme.colors.primery,
  },
  tab_catg_title: {
    fontSize: 10,
    color: Theme.colors.primery,
    textTransform: 'uppercase',
    marginLeft: 5,
  },
  crd_title_header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 5,
    paddingHorizontal: 10,
  },
  header_title: {
    fontSize: 12,
    color: Theme.colors.dark
  },
});
