import React, {useState} from 'react';
// import {styles} from './Style';
import {Text, TouchableOpacity, View} from 'react-native';
import {Theme} from '../../theme/Theme';

const TabView = ({state, descriptors, navigation}) => {
  const [currentTab, setCurrent] = useState(0);
  // useEffect(() => {
  //   if (params) {
  //     setCurrent(params.tab);
  //   }
  //   console.log(params);
  // }, [params]);
  const tabChange = ({i}) => {
    setCurrent(i);
  };
  // console.log(navigation);
  return (
    <View style={{ flexDirection: 'row',}}>
      <View
        style={{
          backgroundColor: 'transparent',
          height: 59,
          flex: 1,
          justifyContent: 'space-evenly',
          alignItems: 'center',
          flexDirection: 'row',
          borderRadius: 14,
          borderColor: Theme.colors.primery,
          borderWidth: 2,
          overflow: 'hidden',
          margin: 24,
          elevation: 0,
          borderBottomWidth: 2,
        }}>
        {state.routes.map((route, index) => {
          const {options} = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
              ? options.title
              : route.name;

          const isFocused = state.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true,
            });

            if (!isFocused && !event.defaultPrevented) {
              // The `merge: true` option makes sure that the params inside the tab screen are preserved
              navigation.navigate({name: route.name, merge: true});
            }
          };

          const onLongPress = () => {
            navigation.emit({
              type: 'tabLongPress',
              target: route.key,
            });
          };
          const focusedTabStyled = {
            backgroundColor: Theme.colors.secondry,
            borderRadius: 12,
            borderWidth: 2,
            borderColor: Theme.colors.primery,
          };
          const focusedTextStyled = {
            color: Theme.colors.white,
            fontWeight: '700',
          };
          return (
            <TouchableOpacity
              key={index}
              accessibilityRole="button"
              accessibilityState={isFocused ? {selected: true} : {}}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              onLongPress={onLongPress}
              style={{
                flex: 1,
                backgroundColor: 'transparent',
                borderTopRightRadius: 12,
                borderBottomRightRadius: 12,
                justifyContent: 'center',
                alignItems: 'center',
                height: 59,
                ...(isFocused ? focusedTabStyled : {}),
              }}>
              <Text
                style={{
                  color: Theme.colors.dark,
                  fontSize: 16,
                  fontWeight: '700',
                  ...(isFocused ? focusedTextStyled : {}),
                }}>
                {label}
              </Text>
            </TouchableOpacity>
          );
        })}
      </View>
    </View>
  );
};
export default TabView;
