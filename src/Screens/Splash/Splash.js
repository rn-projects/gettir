import React from 'react';
import {View, Text, Image} from 'react-native';
import {images} from '../../theme';
import {styles} from './Style';

export default Splash = () => {
  return (
    <View style={styles.splash_main_view}>
      <View style={{flex: 1, justifyContent: 'flex-end'}}>
        <Text style={styles.splash_heading}>Story Time</Text>
      </View>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
        }}>
        <Image
          style={{
            resizeMode: 'contain',
            height: '100%',
          }}
          source={images.splashimg}
        />
      </View>
    </View>
  );
};
