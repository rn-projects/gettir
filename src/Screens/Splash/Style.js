import {StyleSheet} from 'react-native';
import {Theme} from '../../theme/Theme';

export const styles = StyleSheet.create({
  splash_main_view: {
    flex: 1,
    borderWidth: 5,
    borderColor: Theme.colors.lyme,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  splash_heading: {
    fontSize: 40,
    fontWeight: '700',
    color: Theme.colors.lyme,
  },
});
