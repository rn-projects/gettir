import { StyleSheet } from 'react-native'
import { Theme } from '../../theme/Theme'

export const styles = StyleSheet.create({
  signup_main_view: {
    backgroundColor: Theme.colors.white,
    flex: 1,
  },
  signup_content_view: {
    padding: 30,
    flex: 1,
    justifyContent: 'center',
  },
  sign_view: {
    paddingTop: 24,
    paddingHorizontal: 30,
  },
  sign_logo_view: {
    width: 100,
    height: 32,
  },
  sign_logo: {
    height: '100%',
    width: '100%',
    resizeMode: 'contain',
  },
  sign_view_text: {
    fontSize: 10,
    color: Theme.colors.secondry,
    marginTop: 10,
  },
  signup_title_view: {
    marginBottom: 12,
  },
  signup_title: {
    fontSize: 35,
    fontWeight: 'bold',
    color: Theme.colors.dark,
    textTransform: 'capitalize',
  },
  signup_input_view: {
    marginTop: 23,
  },
  frgt_pass_view: {
    display: 'flex',
    flexDirection: 'row',
    marginVertical: 16,
  },
  frgt_text: {
    fontSize: 14,
    opacity: 0.5,
    fontWeight: '500',
  },
  //   reset_tchable: {
  //       backgroundColor: 'red'
  //   },
  frgt_rst_text: {
    color: Theme.colors.primery,
    fontWeight: '700',
  },
  bttn_view: {
    marginVertical: 47,
  },
  // MODAL
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#0006',
  },
  absolute: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  modalView: {
    width: '80%',
    margin: 20,
    backgroundColor: Theme.colors.primery,
    borderRadius: 20,
    padding: 20,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  mdl_title: {
    fontSize: 24,
    color: Theme.colors.white,
    fontWeight: '600',
  },
  mdl_desc: {
    fontSize: 17,
    color: Theme.colors.white,
    textAlign: 'center',
    marginTop: 20,
  },
  mdl_button: {
    borderRadius: 8,
    paddingVertical: 8,
    paddingHorizontal: 38,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  mdl_button_close: {
    marginTop: 30,
    backgroundColor: Theme.colors.white,
  },
  mdl_bttn_text: {
    color: Theme.colors.primery,
    fontWeight: '500',
    textAlign: 'center',
    fontSize: 20,
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
})
