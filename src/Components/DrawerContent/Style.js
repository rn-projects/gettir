import {StyleSheet} from 'react-native';
import {Theme} from '../../theme/Theme';

export const styles = StyleSheet.create({
  drawer_main_view: {
    flex: 1,
    paddingVertical: 30,
    paddingHorizontal: 24,
    backgroundColor: Theme.colors.secondry,
  },
  side_menu_icon_view: {
      marginBottom: 40
  },
  side_menu_icon_opcity: {
    height: 32,
    width: 32,
    borderWidth: 1,
    borderColor: Theme.colors.white,
    borderRadius: 5,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  side_menu_icon: {
      fontSize: 20,
      color: Theme.colors.white
  },
  logout_link_view: {
      display: 'flex',
      flexDirection: "row",
      alignItems: 'center',
      marginLeft: 20
  },
  logout_link: {
    color: Theme.colors.white, 
    fontSize: 18, 
    fontWeight: 'bold',
  },
  logout_icon: {
      color: Theme.colors.white,
      fontSize: 20,
      marginLeft: 15
  },
  version_text: {
      color: Theme.colors.white,
      fontSize: 12,
      textAlign: 'right',
      marginTop: 60
  },

});
