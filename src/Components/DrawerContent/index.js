import React from 'react'
import { SafeAreaView, View, Text, TouchableOpacity } from 'react-native'

import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer'
import { styles } from './Style'
import { Icon } from '../Icon/Icons'
import { Theme } from '../../theme/Theme'
import { connect } from 'react-redux'
import { logoutUser } from '../../store/Actions'

const CustomDrawerContent = ({ navigation, props, logout }) => {
  return (
    <SafeAreaView style={styles.drawer_main_view}>
      <View style={styles.side_menu_icon_view}>
        <TouchableOpacity
          onPress={() => navigation.toggleDrawer()}
          style={styles.side_menu_icon_opcity}>
          <Icon style={styles.side_menu_icon} name="bars" type="fontawesome" />
        </TouchableOpacity>
      </View>
      <DrawerContentScrollView
        contentContainerStyle={{ justifyContent: 'space-between', flex: 1 }}
        {...props}>
        {/* <DrawerItemList {...props} /> */}
        <View style={styles.link_view}>
          <DrawerItem
            labelStyle={{
              color: Theme.colors.white,
              fontSize: 18,
              fontWeight: 'bold',
              opacity: 0.5,
            }}
            label="Profile"
          />
          <DrawerItem
            onPress={() => navigation.navigate('Trade')}
            labelStyle={{
              color: Theme.colors.white,
              fontSize: 18,
              fontWeight: 'bold',
            }}
            label="Trade"
          />
          <DrawerItem
            onPress={() => navigation.navigate('Summary')}
            labelStyle={{
              color: Theme.colors.white,
              fontSize: 18,
              fontWeight: 'bold',
            }}
            label="Summary"
          />
        </View>
        <View style={styles.link_view}>
          <DrawerItem
            labelStyle={{
              color: Theme.colors.white,
              fontSize: 18,
              fontWeight: 'bold',
              opacity: 0.5,
            }}
            label="About"
          />
          <DrawerItem
            labelStyle={{
              color: Theme.colors.white,
              fontSize: 18,
              fontWeight: 'bold',
              opacity: 0.5,
            }}
            label="Terms of Service"
          />
          <DrawerItem
            labelStyle={{
              color: Theme.colors.white,
              fontSize: 18,
              fontWeight: 'bold',
              opacity: 0.5,
            }}
            label="Privacy Policy"
          />
          <TouchableOpacity style={styles.logout_link_view} onPress={logout}>
            <Text style={styles.logout_link}>Logout</Text>
            <Icon style={styles.logout_icon} name="logout" type="antdesign" />
          </TouchableOpacity>
        </View>
      </DrawerContentScrollView>
      <Text style={styles.version_text}>version - 1.1</Text>
    </SafeAreaView>
  )
}

function mapDispatchToProps(dispatch) {
  return {
    logout: () => dispatch(logoutUser()),
  }
}
export default connect(null, mapDispatchToProps)(CustomDrawerContent)
