import React from 'react';
import {Text, View} from 'react-native';
import {styles} from './Style';
import {Icon} from '../Icon/Icons';

const AppHeader = ({icon, title,title1, title2 }) => {
  return (
    <View style={styles.header_view}>
      {icon && <Icon style={styles.head_icon} name={icon} />}
      {title && <Text style={styles.head_title}>{title}<Text style={styles.head_title1}>{title1}</Text></Text>}
      {title2 && <Text style={styles.head_title2}>{title2}</Text>}
      <View style={styles.emty_view}/>
    </View>
  );
};
export default AppHeader;
