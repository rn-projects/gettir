import {StyleSheet} from 'react-native';
import {Theme} from '../../theme/Theme';
import {elevation} from "../../utils/elevation";

export const styles = StyleSheet.create({
  header_view: {
    backgroundColor: Theme.colors.primery,
    height: 60,
    flexDirection: 'row',
    paddingHorizontal: 20,
    alignItems: 'center',
    justifyContent: 'space-between',
    ...elevation(5)
  },
  head_icon: {
    fontSize: 22,
    color: Theme.colors.white,
  },
  head_title: {
    fontSize: 25,
    fontWeight: 'bold',
    color: Theme.colors.secondry,
  },
  head_title1: {
    fontWeight: 'bold',
    color: Theme.colors.white,
  },
  head_title2: {
      fontSize: 20,
    fontWeight: 'bold',
    color: Theme.colors.white,
  },
  emty_view: {
    width: 20,
  },
});
