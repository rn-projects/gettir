import {StyleSheet} from 'react-native';
import {Theme} from '../../theme/Theme';
import {elevation} from "../../utils/elevation";

export const styles = StyleSheet.create({
    app_bttn: {
        backgroundColor: Theme.colors.primery,
        borderRadius: 8,
        height: 55,
    },
    app_bttn_text: {
        color: Theme.colors.white,
        fontSize: 15,
        fontWeight: 'bold',
        textTransform: 'capitalize'
    },
});
