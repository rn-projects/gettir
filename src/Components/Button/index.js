import React from 'react';
import {Text, View} from 'react-native';
import {styles} from './Style';
import {Icon} from '../Icon/Icons';
import {Button} from 'native-base';
import {Theme} from '../../theme/Theme';

const AppButton = ({
  title,
  backgroundColor,
  transparent,
  bordered,
  borderColor,
  borderWidth,
}) => {
  return (
    <Button
      transparent={transparent}
      bordered={bordered}
      style={[styles.app_bttn, {backgroundColor, borderWidth, borderColor}]}
      block>
      <Text style={styles.app_bttn_text}>{title}</Text>
    </Button>
  );
};
export default AppButton;
