import React from 'react'
import {
  AntDesign,
  Entypo,
  EvilIcons,
  Feather,
  FontAwesome,
  Ionicons,
  MaterialIcons,
} from './IconSet'

export const Icon = ({ type, ...props }) => {
  switch (type) {
    case 'antdesign':
      return <AntDesign {...props} />
    case 'entypo':
      return <Entypo {...props} />
    case 'evilicons':
      return <EvilIcons {...props} />
    case 'feather':
      return <Feather {...props} />
    case 'ionicons':
      return <Ionicons {...props} />
    case 'materialicons':
      return <MaterialIcons {...props} />
  }
  return <FontAwesome {...props} />
}
